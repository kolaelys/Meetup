<?php
class Db {
    public static function connexion(){
        include_once('config.php');
        // connection
        try{

                $bdd = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password);
                return $bdd;
        }

        catch (Exception $e){

                die('Erreur : ' . $e->getMessage());
        }
    }
}
?>