<?php
include_once("connexion/Db.php");

class Location extends Db {
    private $address;
    private $city;
    private $zip_code;
    private $description;

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getCity() {
        return $this->city;
    }

    public function setLastCity($city) {
        $this->city = $city;
    }

    public function getZipCode(){
        return $this->zip_code;
    }

    public function setZipCode(){
        $this->zip_code = $zip_code;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getAll(){
        $bdd=Db::connexion();
        $list= $this->db->prepare("SELECT * FROM location");
    }

    function addLocation($id,$address, $city, $zip_code, $description){
        $bdd=Db::connexion();
        $req=$bdd->prepare('INSERT INTO location (`id`,`address`, `city`, `zip_code`, `description`) VALUES (:id, :address, :city, :zip_code, :description)');
        $req->execute(array(
            'id'=> $id,
            'address'=>$address,
            'city'=>$city,
            'zip_code'=>$zip_code,
            'description'=>$description
        ));
    }

    public function deleteLocation($n){
        $bdd=Db::connexion();
        $req=$bdd->prepare('DELETE FROM location WHERE `id`= ?');
        $req->execute(array($n));
        echo "done !!";
        }

    public function updateLocation($index,$address,$city,$zip_code,$description){
            $bdd=Db::connexion();
            $req=$bdd->prepare('UPDATE location SET address = :address, city= :city, zip_code= :zip_code, description :description   WHERE `id` = :id' );
            $req->execute(array('id'=> $index,'address'=> $address, 'city'=>$city, 'zip_code'=>$zip_code, 'description'=>$description));
            echo "done !!";
        }
}
