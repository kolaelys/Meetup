<?php
include_once(__DIR__."/../connexion/Db.php");

class Meetup extends Db {
    private $title;
    private $description;
    private $img;
    private $date;

    public function __construct($title, $description, $img, $date){
        $this->setTitle($title);
        $this->setDescription($description);
        $this->setImg($img);
        $this->setDate($date);
    }


     public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getImg(){
        return $this->img;
    }

    public function setImg(){
        $this->img = $img;
    }

    public function getDate() {
        return $this->date;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function getId(){
        return $this->id;
    }

    public function getUserId(){
        return $this->UserId;
    }
    public static function getAll(){
        $bdd=Db::connexion();
        $list= $bdd->query("SELECT * FROM meetup");
        return $list->fetchAll();
    }

    public static function getWithId($id){
        $bdd=Db::connexion();
        $list= $bdd->query("SELECT * FROM meetup where id = " . $id);
        return $list->fetch();
    }

    static function addMeetup($title, $description, $img, $date){
        $bdd=Db::connexion();
        $req=$bdd->prepare('INSERT INTO meetup (`title`,`description`, `img`,`date`) VALUES (:title, :description, :img, :date)');
        $req->execute(array(
            'title'=>$title,
            'description'=>$description,
            'img'=>$img,
            'date'=>$date
        ));
    }

    public static function getLastMeetup(){
        $bdd=Db::connexion();
        $lastMeetup=$bdd->query('SELECT * FROM meetup ORDER BY id DESC LIMIT 1');
        return $lastMeetup->fetch();
    }

    public static function deleteMeetup($n){
        $bdd=Db::connexion();
        $req=$bdd->prepare('DELETE FROM meetup WHERE id = ?');
        $req->execute(array($n));
        }

    public static function updateMeetup($index,$title,$description,$img, $date){
            $bdd=Db::connexion();
            $req=$bdd->prepare('UPDATE meetup SET title = :title, description= :description, img= :img, date= :date   WHERE `id` = :id' );
            $req->execute(array('id'=> $index,'title'=> $title,'description'=>$description, 'img'=>$img, 'date'=>$date ));
            // echo "done !";
        }
       
}
