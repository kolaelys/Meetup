<?php
include_once("connexion/Db.php");

class Speaker extends Db {
    private $firstName;
    private $lastName;
    private $description;

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }


    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getAll(){
        $list= $this->db->prepare("SELECT * FROM speaker");
    } 

    function addSpeaker($id, $firstName, $lastName, $description){
        $bdd=Db::connexion();
        $req=$bdd->prepare('INSERT INTO speaker (`id`,`firstName`, `lastName`, `description`) VALUES (:id, :firstName, :lastName, :description)');
        $req->execute(array(
            'id'=> $id,
            'firstName'=>$firstName,
            'lastName'=>$lastName,
            'description'=>$description
        ));
    }

    public function deleteSpeaker($n){
        $bdd=Db::connexion();
        $req=$bdd->prepare('DELETE FROM `speaker` WHERE `id`= ?');
        $req->execute(array($n));
        echo "done !!";
        }

    public function updateSpeaker($index,$firstName,$lastName,$description){
            $bdd=Db::connexion();
            $req=$bdd->prepare('UPDATE speaker SET firstName = :firstName, lastName= :lastName, description :description   WHERE `id` = :id' );
            $req->execute(array('id'=> $index,'firstName'=> $firstName, 'lastName'=>$lastName, 'description'=>$description));
            echo "done !!";
        }
}
