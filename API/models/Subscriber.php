<?php
include_once("connexion/Db.php");

class Subscriber {
    private $firstName;
    private $lastName;
    private $mailAddr;
    private $dateSubscription;

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getMailAddr() {
        return $this->mailAddr;
    }

    public function setMailAddr($mailAddr) {
        $this->mailAddr = $mailAddr;
    }

    public function getDateSubscription() {
        return $this->dateSubscription;
    }

    public function setDateSubscription($dateSubscription) {
        $this->dateSubscription = $dateSubscription;
    }

    public function getAll() {
        // Ici votre requête SQL (peut-être qu'un extends serait sympa pour la connexion en DB...)
        // Les données ci-dessous son fakes !
        $listSubscriber= $this->db->prepare("SELECT * FROM subscriber");
    }

    function addSubscriber($id, $firstName, $lastName, $mailAddr, $dateSubscription ){
        $bdd=Db::connexion();
        $req=$bdd->prepare('INSERT INTO subscriber (`id`,`firstName`, `lastName`, `mailAddr`, `dateSubscription`) VALUES (:id, :firstName, :lastName, :mailAddr, :dateSubscription)');
        $req->execute(array(
            'id'=> $id,
            'firstName'=>$firstName,
            'lastName'=>$lastName,
            'mailAddr'=>$mailAddr,
            'dateSubscription'=>$dateSubscription
        ));
    }

    public function deleteSubscriber($n){
        $bdd=Db::connexion();
        $req=$bdd->prepare('DELETE FROM `subscriber` WHERE `id`= ?');
        $req->execute(array($n));
        echo "done !!";
        }

    public function updateSubscriber($index,$firstName,$lastName,$mailAddr, $dateSubscription){
            $bdd=Db::connexion();
            $req=$bdd->prepare('UPDATE subscriber SET firstName = :firstName, lastName= :lastName, mailAddr :mailAddr, dateSubscription :dateSubscription WHERE `id` = :id' );
            $req->execute(array('id'=> $index,'firstName'=> $firstName, 'lastName'=>$lastName, 'mailAddr'=>$mailAddr, 'dateSubscription'=>$dateSubscription));
            echo "done !!";
        }
}
