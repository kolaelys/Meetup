<?php

require_once 'models/Subscriber.php';

class SubscriberController {
    public function getAll() {
        $subscriber = new Subscriber();
        echo json_encode($subscriber->getAll());
    }

    public function getWithId($id) {
        echo json_encode('Mon id en param : ' . $id);
    }
}