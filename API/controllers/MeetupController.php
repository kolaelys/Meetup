<?php

require_once  __DIR__.'/Controller.php';
require_once  __DIR__."/../models/Meetup.php";

class MeetupController extends Controller{

    
    function getAll() {
        $all= json_encode(Meetup::getAll());
        echo $all;
    }

    function getWithId($id) {
        return json_encode(Meetup::getWithId($id));
    }

    function getLastMeetup(){
        $lastMeetup= json_encode(Meetup::getLastMeetup());
        echo $lastMeetup;
    }
    
    function getHttpData() {
        // GET
        if ($_SERVER['REQUEST_METHOD'] == 'GET') return $_GET;
        // POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') return $_POST;
        // PUT / PATCH / DELETE
        parse_str(file_get_contents("php://input"),$data);
        return $data;
     }
     

    public function addMeetup(){
        $title = $_POST['title'];
        $description = $_POST['description'];
        $img = $_FILES['img'];
        $imgName = preg_replace('/([^.a-z0-9]+)/i','-',$img['name']);
        $imgUniqId =  time().uniqid(rand()).$imgName;
        $this->checkImg();
        $this->check_extensionImg();
        $this->transferImg($imgUniqId);
        $date= $_POST['date'];
        Meetup::addMeetup($title, $description, $imgUniqId, $date);
    } 

    public function checkImg(){

        //L'utilisateur n'a pas envoyé de fichier
    
            if (empty($_FILES['img'])) {
                $message= ('Vous n\'avez rien selectionné');
            }
    
            if($_FILES['img']['error']>0)$error="Erreur lors du transfert"; 
        }
    
        //Verification extension et taille du fichier 
        
        public function check_extensionImg(){
     
            $extensionsValides=['jpg','jpeg','png','svg','gif'];
            $pathInfo=pathinfo($_FILES['img']['name']);
            $extension= strtolower($pathInfo['extension']);
            $maxsize= 10000000;
            if(in_array($extension, $extensionsValides)){
                echo "Extension correct";
            }
            else{
                echo "extension incorrecte";
            }
        
            if($_FILES['img']['size']>$maxsize)$erreur='le fichier est trop gros';
        }

    public function transferImg($name){
        $img = $_FILES['img'];
        $imgName = preg_replace('/([^.a-z0-9]+)/i','-',$img['name']);
        $destination = '../API/pictures/'. $name;
        $resultat = move_uploaded_file($_FILES['img']['tmp_name'], $destination);
        if ($resultat) {

            $message= ("Le fichier' .$imgName. 'a bien été téléchargé :)");
        }
    }


        public function updateMeetup(){
        $data = $this->getHttpData();
        $index = $data['id'];
        $title = $data['title'];
        $description = $data['description'];
        $img = $data['img'];
        $date= $data['date'];
        Meetup::updateMeetup($index, $title, $description, $img, $date);
        
    }

   /* public function deleteMeetup(){
        $id = isset($_POST['id']) ? $_POST['id'] : NULL;
        Meetup:: deleteMeetup($id);
    }*/

    public function deleteMeetup(){
        $data = $this->getHttpData();
        $id = isset($data['id']) ? $data['id'] : NULL;
        Meetup::deleteMeetup($id);
    }

    
    
}  
