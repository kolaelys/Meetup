<?php

require_once 'models/Speaker.php';

class SpeakerController{
    function getAll() {
        $speaker = new Speaker();
        echo json_encode($speaker->getAll());
    }

    function getWithId($id) {
        echo json_encode('Speaker'.$id);
    }
}