<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:38
 */

use Pecee\SimpleRouter\SimpleRouter;
require_once 'APIMiddleware.php';
require_once 'controllers/LoginController.php';
require_once 'controllers/DefaultController.php';
require_once 'controllers/SubscriberController.php';
require_once 'controllers/SpeakerController.php';
require_once 'controllers/LocationController.php';
require_once 'controllers/MeetupController.php';


// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
// http://localhost/simplon/routeur/
$prefix = '/Meetup/API';

SimpleRouter::group(['prefix' => $prefix], function () {
    SimpleRouter::post('/login', 'LoginController@login');
    SimpleRouter::get('/login', 'LoginController@error')->name('login');

    // SimpleRouter::get('/', 'DefaultController@defaultAction');

    // SimpleRouter::get('/', 'MeetupController@getAll');
    SimpleRouter::get('/meetups', 'MeetupController@getAll');
    // SimpleRouter::get('/meetup', 'MeetupController@getLastMeetup');

    SimpleRouter::post('/addMeetup', 'MeetupController@addMeetup');
    SimpleRouter::delete('/meetup', 'MeetupController@deleteMeetup');
    SimpleRouter::get('/meetup/{id}', 'MeetupController@getWithId');
    SimpleRouter::patch('/update', 'MeetupController@updateMeetup');

    // SimpleRouter::get('/meetup/{id}', 'MeetupController@getWithId');
  

    SimpleRouter::get('/subscribers', 'SubscriberController@getAll')->addMiddleware(APIMiddleware::class);
    SimpleRouter::post('/subscriber/{id}', 'SubscriberController@getWithId')->addMiddleware(APIMiddleware::class);

    SimpleRouter::get('/speakers', 'SpeakerController@getAll')->addMiddleware(APIMiddleware::class);
    SimpleRouter::post('/speaker/{id}', 'SpeakerController@getWithId')->addMiddleware(APIMiddleware::class);


    

    SimpleRouter::get('/locations', 'LocationController@getAll');
    SimpleRouter::post('/location/{id}', 'LocationController@getWithId');
    SimpleRouter::post('/location/{id}', 'LocationController@addLocation');
    SimpleRouter::post('/location/{id}', 'LocationController@deleteLocation');
    SimpleRouter::post('/location/{id}', 'LocationController@updateLocation');
   

});