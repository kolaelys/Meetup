$addButton.on('click', function(){
    // données formulaire
    var data  = {
        token: localStorage.getItem('token')
    }
    // afficher le loader
    startLoading()
    // ajax 
   $.ajax( {
            url: "/exercice-token/meetups/2",
            method: "POST",
            data: data
        })
        .done(editSuccess) // si le login fonctionne
        .fail(editFail) // si le login échoue
        .always(stopLoading) // dans tous les cas
 })

// quand l'édition du titre fonctionne
function editSuccess(result) {
    toastr.success(`Modification sauvegardée`)
}

// quand l'édition du titre échoue
function editFail(error){
    var message = JSON.parse(error.responseText).message;
    toastr.error(`Erreur : ${message}`)
    console.log(error)
}
