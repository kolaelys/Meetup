$(document).on('click', ".upDirect", function () {

    var id = $(this).attr("data-id");
    
    $.ajax({
        method: "GET",
        url: "API/meetup/" + id,
      
    })
        .done(function (element){
                element = JSON.parse(element);
               
                $("#mainContent").empty();
                $("#mainContent").append(
                    '<div class="UpForm" name="update">' +
                    '<input type="hidden" name="id" value="' + element.id + '">' +
                    'Titre: <input class=" update title-' + element.id + '" type="text" name="title" value="' + element.title + '">' +
                    'Description: <input class="update description-' + element.id + '"type="texte" name="description" value="' + element.description + '">' +
                    ' <input type="hidden" src="../API/pictures" class="img-' + element.id + '" name="img" value=' + element.img + '>' +
                    'Date: <input class="update date-' + element.id + '" name="date" value="' + element.date + '"><br/>' +
                    '<img src="API/pictures/' + element.img + '" id="photoProfil" style= "width:150px; height: 100px" class="avatar img-circle" alt="image">' +
                    '<input class="modif updateSubmit" data-id="' + element.id + '" type="image" src="img/update.png" value="submit" >\n' +
                    '<br/><br/>' +
                    '</div>')
            })
        })







/************* FORMULAIRE AJOUT UN MEETUP ***************/
$("#btnAdd").click(function () {
    $("#mainContent").empty();
    $("#mainContent").html('<form id="addMeetup" enctype="multipart/form-data">' +
        '<ul>' +
        '<li>' +
        '<label for="title">Titre</label>' +
        '<span>Quelle est le titre du nouveau Meetup?</span><br/>' +
        ' <input type="text" name="title" maxlength="100">' +

        '</li>' +
        '<li>' +
        ' <label for="description">Description</label>' +
        '<span>Une description?</span><br/>' +
        '<textarea type="text" name="description" ></textarea>' +

        '</li>' +
        '<li>' +
        '<label for="img">Illustration</label>' +
        '<span>ça ressemble à quoi?</span><br/>' +
        '<input type="file" name="img">' +

        '</li>' +
        '<li>' +
        '<label for="date">Date</label>' +
        "<span>Quand l'évènement a-t-il lieu?</span><br/>'" +
        '<input type="date" name="date"/>' +

        '</li>' +
        '<li>' +
        '<input class="addMeetup" type="image" src="img/add.png" name="submit" value="Ajouter un Meetup">' +
        '</li>' +
        '</ul>' +
        '</form>')

})


//LISTE MEETUPS

$("#btnList").click(function () {
    $("#mainContent").empty();
    // $("#mainContent").html('<h2>MEETUP PROGRAMMES</h2>')
    $.ajax({
        method: "GET",
        url: "API/meetups"
    })
        .done(function (msg) {
           
            json = JSON.parse(msg);
            //   $(".meetups").append('<div class="meetup">'+json.title+ "<br/>"+json.description+'</div>');

            json.forEach(function (element) {
                $("#mainContent").prepend(

                    '<div id="listMeetups" class="meetup btn draw-border">' +
                    // "id: "+element.id+"<br/>"+ 

                    '<span class="title">' + element.title + '</span><br/>' +
                    '<span class="description"> description:' + element.description + '</span><br/>' +
                    // "img: "+element.img+
                    '<img class="img" src="API/pictures/' + element.img + '" id="photoProfil" style= "width:150px; height: 100px" class="avatar img-circle" alt="image">' + "<br/>" +
                    '<span class="date">date: ' + element.date + '</span>' +
                    'Update<input class="modif upDirect" data-id="' + element.id + '" type="image" src="img/update.png" value="submit" >\n' +
                    'Delete<input class="modif delete" data-id="' + element.id + '" type="image" src="img/delete.png" value="submit">' +
                    '</div>'
                );
            })
        })
})


/*********** UPDATE MEETUP ************/


$(document).on('click', ".updateSubmit", function () {
    var id = $(this).data('id');
    var title = $('.title-' + id).val();
    var description = $('.description-' + id).val();
    var img = $('.img-' + id).val();
    var date = $('.date-' + id).val();
    $.ajax({
        type: 'PATCH',
        data: { id: id, title: title, description: description, img: img, date: date },
        url: "API/update",
    }).done((function() {
        toastr.success('Vous pouvez retourner sur la liste des meet-up :)', 'Mise à jour effectuée!');
   })).fail((function(){
    toastr.error ('Une <b>erreur</b> s\'est produite', 'Erreur');
   }))
    return false;
});


/*********** DELETE MEETUP **************/


$(document).on('click', ".delete", function () {
    var id = $(this).data('id');
   
    $.ajax({
        type: 'DELETE',
        url: "API/meetup",
        data: { id: id },
    }).done((function() {
        toastr.success('N\'hésitez pas à rajouter un meetup-up pour compenser ;)' , 'Suppression ok!');
   })).fail((function(){
    toastr.error ('Une <b>erreur</b> s\'est produite', 'Erreur');
   }))
    return false;
});

/************ ADD MEETUP *************/

$('body').on('submit', '#addMeetup', function () {
    var form = $('form').get(0);
    var formData = new FormData(form);
    $.ajax({
        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url: 'API/addMeetup', // the url where we want to POST
        data: formData, // our data object
        dataType: 'json', // what type of data do we expect back from the server
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
    }).done((function() {
        toastr.success('Vous pouvez retourner sur la liste des meet-up :)', 'Ajout effectuée!');
   })).fail((function(){
    toastr.error ('Une <b>erreur</b> s\'est produite', 'Erreur');
   }))
    return false;
});

